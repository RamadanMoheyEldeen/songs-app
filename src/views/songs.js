import React from 'react';
import Song from '../components/song'
import { actions } from "../store/actions";
import { useDispatch, useSelector } from "react-redux";

const Songs = () => {
    const dispatch = useDispatch();
    const { songs, songsType, isLoading } = useSelector((state) => state.songs);
    const [scrollTop, setScrollTop] = React.useState(0);
    const [limit, setLimit] = React.useState(0);

    React.useEffect(()=>{
        const onScroll = e => {
            if( (window.innerHeight + window.scrollY) >= document.body.offsetHeight)
                {
                    dispatch(actions.songs.fetchSongsHandler({_start: limit , _end: limit + 10 }));
                    setLimit(limit + 10);
                }

            setTimeout(()=>{
                setScrollTop(e.target.documentElement.scrollTop);
            })
        };
        if(songsType === 'songs')
          window.addEventListener("scroll", onScroll);
        return () => window.removeEventListener("scroll", onScroll);
    }, [scrollTop, songs])

    React.useEffect(() => {
        dispatch(actions.songs.fetchSongsHandler({_start: 0, _end: 10}));
        setLimit(limit + 10);
    }, [dispatch]);
    
    return (
        <div className="songs-container">
            {
               songs.map(song => {
                    return <Song key={song.id} {...song} />
                })
            }
            {
                isLoading && <div className="loader-container center">
                    <div className="loader"></div>
                </div>
            }
        </div>
    )
}

export default Songs;