import React from 'react';
import AddSongForm from '../components/addSongForm';

const AddSong = () => {
    return (
        <AddSongForm />
    )
}

export default AddSong;