import Axios from "../axios-config";

export const getSongs = (params, qString="") =>  Axios.get(`/songs/?${qString}`, {params});

export const addSong = (body) =>  Axios.post(`/songs`, body);

export const addToFavorite = (body) =>  Axios.post(`/favorites`, body);

export const removeFormFavorite = (id) =>  Axios.delete(`/favorites/${id}`);

export const isFavoriated = (id) => Axios.get(`/favorites/${id}`);