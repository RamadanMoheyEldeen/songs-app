import axios from "axios";

const Axios = axios.create({
  // @ts-ignore
  Accept: "application/json",
  baseURL: "http://localhost:3004",
});

export default Axios;
