import React from 'react';
import Filter from './filter'
import { useHistory } from 'react-router-dom';

const FilterContainer = () => {
    const history = useHistory();
    const addSongHandler = async () => {
        history.push("/add-song");
    }
    return (
        <div className="filter-container space-between flex-end">
            <Filter />
            <button onClick={() => addSongHandler()} className="add-btn"> Add a song </button>
        </div>
    )
}
export default FilterContainer;