import React from 'react';
import ReactStars from "react-rating-stars-component";

const Stars = ({onChangeRaiting, value}) => {
    return (
        <ReactStars
            count={5}
            onChange={onChangeRaiting}
            size={26}
            value={value}
            classNames="stars-container"
            edit={value > 0 ? false : true}
            isHalf={true}
            emptyIcon={<i className="far fa-star"></i>}
            halfIcon={<i className="fa fa-star-half-alt"></i>}
            filledIcon={<i className="fa fa-star"></i>}
            activeColor="#5844AF"
        />
    )
}

export default Stars;

