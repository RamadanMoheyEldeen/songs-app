import React, { useState } from 'react';
import { actions } from "../store/actions";
import { useSelector, useDispatch } from "react-redux";
import { addToFavorite, removeFormFavorite, isFavoriated } from "../api";

const Love = (props) => {
    const { favoriteSongs } = useSelector(state => state.songs);
    const [favorited, setFavoriated] = useState(false);

    const dispatch = useDispatch();
    
    const onLike = async () => {
        dispatch(actions.songs.addToFavorite(props.id))
        await addToFavorite({...props});        
        isFavoriatedSong();
    }

    const isFavoriatedSong = async () => {
       try {
        const response =  await isFavoriated(props.id);
        if(response?.data?.id)
            setFavoriated(true);
        else 
            setFavoriated(false);
       } catch(err) {

       } finally {

       }
    }

    const onUnLike = async () => {
        await removeFormFavorite(props.id)
        dispatch(actions.songs.removeFromFavorite(props.id));
        setFavoriated(false);
    }

    React.useEffect(()=>{
        isFavoriatedSong();
    }, [])
    return favorited || favoriteSongs.indexOf(props.id) !== -1   ? <i onClick={()=>onUnLike()} className="fa fa-heart"></i> : <i onClick={()=>onLike()} className="far fa-heart"></i>
}


export default Love;