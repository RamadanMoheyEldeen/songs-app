import React from 'react';
import logo from '../assets/images/icon.jpg';
import Search from './search';
import UserData from './userData'

const Header = () => {
    return (
        <header>
            <div className="h-wrapper space-between">
                <span><img src={logo} alt="Logo" /></span>
                <Search/>
                <UserData/>
            </div>
        </header>
    )
}
export default Header;