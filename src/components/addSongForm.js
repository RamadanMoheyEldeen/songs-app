import React from 'react';
import { useForm } from "react-hook-form";
import { isImageUrl, isNumber } from "../helpers";
import { addSong } from "../api";
import { useHistory } from "react-router-dom";

const AddSongForm = () => {
    const history = useHistory();
    const initialValues = {
        title: null,
        artist: null,
        images: null,
        level: null,
        search: null
    };

    const { register, handleSubmit, errors, getValues } = useForm({
        defaultValues: initialValues,
    });

    const onSubmit = async (data) => {
        try {
            const resposnse = await addSong({ ...data, id: (new Date()).valueOf() });
            if (resposnse?.data?.id) {
                history.push("/")
            }
        } catch (err) {

        } finally {

        }
    }

    return (
        <form>
            <div className="form-item">
                <label>Title</label>
                <input name="title" ref={register({required: true})} type="text" placeholder="Title" />
                <span>
                    {errors.title?.type === "required" && (
                        <span>Title is required</span>
                    )}
                </span>
            </div>

            <div className="form-item">
                <label>Artisit </label>
                <input name="artist" ref={register({required: true})} type="text" placeholder="Artist name" />
                <span>
                    {errors.artist?.type === "required" && (
                        <span>Artist is required</span>
                    )}
                </span>
            </div>

            <div className="form-item">
                <label>Image url</label>
                <input name="images" ref={register({
                    required: true,
                    validate: isImageUrl,
                })} type="text" placeholder="Valid image url" />
                <span>
                    {errors.images?.type === "required" && (
                        <span>Image url is required</span>
                    )}
                    {errors.images?.type === "validate" && (
                        <span>Text is not a valid image url</span>
                    )}
                </span>
            </div>

            <div className="form-item">
                <label>Level</label>
                <input name="level" ref={register({
                    required: true,
                    validate: isNumber,
                })} type="text" placeholder="Level" />
                <span>
                    {errors.level?.type === "required" && (
                        <span>Level is required</span>
                    )}
                    {errors.level?.type === "validate" && (
                        <span>Level should be a number </span>
                    )}
                </span>
            </div>

            <div className="form-item">
                <label>Search text</label>
                <input name="search"  ref={register({required: true})} type="text" placeholder="Search text" />
                <span>
                    {errors.search?.type === "required" && (
                        <span>Search text is required</span>
                    )}
                </span>
            </div>

            <div className="form-item">
                <input onClick={handleSubmit(onSubmit)} type="submit" value="Add Song" />
            </div>

        </form>
    )
}

export default AddSongForm;