import React from 'react';

const UserData = () => {
    return (
        <div className="u-wrapper space-between">
            <div className="avatar center">
                <i className="fa fa-user"></i>
            </div>
            <span className="welcome"> Welcome to ViuLive</span>
        </div>
    )
}

export default UserData;