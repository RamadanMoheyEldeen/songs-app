import React from 'react';
import Stars from './stars';
import Love from './love';
import {getStars} from "../helpers/stars"

const Song = (props) => {    
    return (
        <div className="song">
            <img className="song-image" src={props.images} loading="lazy" alt="song" />
            <div className="song-content">
                <div className="titles flex-column">
                    <h3>{props.title}</h3>
                    <small>{props.artist}</small>
                </div>
                <div className="raiting-love space-between">
                    <Stars onChangeRaiting={() => { }} value={getStars(props.level)} />
                    <Love {...props} />
                </div>
            </div>
        </div>
    )
}

export default Song;