import React from 'react';
import { actions } from "../store/actions";
import { useDispatch } from "react-redux";

const Search = () => {
    const dispatch = useDispatch();
    const [text, setSearchText]= React.useState("");
    
    const onChangeHandler = () => {
        if(text)
           dispatch(actions.songs.searchSongsHandler(`search_like=${text}`));
    }

    return (
        <div className="s-wrapper space-between">
            <input onChange={(e)=> setSearchText(e.target.value)} placeholder="Search..." />
            <i onClick={()=>{onChangeHandler()}} className="fa fa-search"></i>
        </div>
    )
}

export default Search;