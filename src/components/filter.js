import React from 'react';
import Stars from './stars';
import {actions} from "../store/actions"
import { useDispatch } from "react-redux";
import { getLevels } from "../helpers/stars";

const Filter = () => {
    const dispatch = useDispatch();
    const onChangeRaiting = (newRating) => {      
        const levelQuery = getLevels(newRating)
        dispatch(actions.songs.filterSongsHandler(levelQuery));
    };


    return (
            <div className="fs-wrapper flex-column">
                Filter by stars 
                <Stars value={0} onChangeRaiting={onChangeRaiting} />
            </div>
    )
}
export default Filter;