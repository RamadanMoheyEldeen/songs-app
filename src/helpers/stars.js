export const getStars = (level) => {
    if (level % 3 === 0)
        return level / 3;
    else
        if (((level - 1) % 3 === 0))
            return (level - 1) / 3 + .5;
        else if (((level - 2) % 3 === 0))
            return (level - 2) / 3 + .5;
}

export const getLevels = (rate) => {
    if(Number.isInteger(rate))
        return `level=${rate * 3}`;
    else 
        return `level_like=${(parseInt(rate) * 3 + 1)}&level_like=${parseInt(rate) * 3 + 2}`
}