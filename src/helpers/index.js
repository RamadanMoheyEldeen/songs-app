export const isImageUrl = url => (url.match(/\.(jpeg|jpg|gif|png)$/) != null);

export const isNumber = number => !isNaN(number);