import {
  fetchSongsHandler,
  filterSongsHandler,
  searchSongsHandler,
  addToFavorite, 
  removeFromFavorite,
} from "./songs/action";


export const actions = {
  songs: {
    fetchSongsHandler,
    filterSongsHandler,
    addToFavorite,
    removeFromFavorite,
    searchSongsHandler
  }
};
