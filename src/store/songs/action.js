import { getSongs } from "../../api"

export const FETCH_SONGS  =  "FETCH_SONGS";
export const FILTER_SONGS = "FILTER_SONGS";
export const SEARCH_SONGS = "SEARCH_SONGS";
export const SHOW_LOADER  =  "SHOW_LOADER";
export const ADD_TO_FAVORITE = "ADD_TO_FAVORITE";
export const REMOVE_FROM_FAVORITE = "REMOVE_FROM_FAVORITE";

export function fetchSongs(
  payload
) {
  return {
    type: FETCH_SONGS,
    payload,
  };
}


export function filterSongs(
  payload
) {
  return {
    type: FILTER_SONGS,
    payload,
  };
}

export function searchSongs (
  payload
) {
  return {
    type: SEARCH_SONGS,
    payload
  }
}

export function showLoader(
  payload
) {
  return {
    type: SHOW_LOADER,
    payload,
  };
}

export function addToFavorite(
  payload
) {
  return {
    type: ADD_TO_FAVORITE,
    payload,
  };
}

export function removeFromFavorite(
  payload
) {
  return {
    type: REMOVE_FROM_FAVORITE,
    payload,
  };
}

export function searchSongsHandler(query) {
  return async (dispatch) => {
    try {
        dispatch(showLoader(true))
        const response= await getSongs({}, query);
        dispatch(searchSongs(response.data));
    } catch {} finally {
      dispatch(showLoader(false))
    }
  };
}

export function filterSongsHandler(query) {
  return async (dispatch) => {
    try {
        dispatch(showLoader(true))
        const response= await getSongs({},query);
        dispatch(filterSongs(response.data));
    } catch {} finally {
      dispatch(showLoader(false))
    }
  };
}

export function fetchSongsHandler(params) {
  return async (dispatch) => {
    try {
        dispatch(showLoader(true))
        const response= await getSongs(params);
        dispatch(fetchSongs(response.data));
    } catch {} finally {
      dispatch(showLoader(false))
    }
  };
}