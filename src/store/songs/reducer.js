import {
  FETCH_SONGS,
  FILTER_SONGS,
  SHOW_LOADER,
  ADD_TO_FAVORITE,
  REMOVE_FROM_FAVORITE,
  SEARCH_SONGS
} from "./action";

const initialState = {
  songs: [],
  songsType: 'songs',
  favoriteSongs: [],
  isLoading: true,
}

export default function initialData(state = initialState, action) {
  switch (action.type) {
    case FETCH_SONGS:
      return { ...state, songsType: 'songs', songs: [...state.songs, ...action.payload] };
    case FILTER_SONGS:
      return { ...state, songsType: 'filtered', songs: action.payload };
    case SEARCH_SONGS: 
      return { ...state, songsType: 'search', songs: action.payload };
    case SHOW_LOADER:
      return { ...state, isLoading: action.payload };
    case ADD_TO_FAVORITE:
      return { ...state, favoriteSongs: [...state.favoriteSongs, action.payload ]};
    case REMOVE_FROM_FAVORITE:
      return { ...state, favoriteSongs: state.favoriteSongs.filter(id => id !== action.payload) };
    default:
      return state;
  }
}
