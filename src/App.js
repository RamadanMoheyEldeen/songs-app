
import Header from './components/header';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import FilterContainer from './components/filterContainer';
import Songs from './views/songs';
import AddSong from './views/addSong';
import './assets/styles/App.css';


function App() {
  return (
    <div className="App">
      <Header />
      <div className="content flex-column">
        <Router>
          <Switch>
            <Route exact path={`/`}>
              <FilterContainer />
              <Songs />
            </Route>
            <Route path={`/add-song`}>
              <AddSong />
            </Route>
          </Switch>
        </Router>
      </div>
    </div>
  );
}

export default App;
